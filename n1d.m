%NuclearNrMinNoisy rankone
% matrix completion
close all
clearvars
Interval=40;%:20:70;

for rate=Interval
    rate
    for k=1:10
        k
        % Generating read matrix
        l=30;N=30;
        h=2*(rand(l,1)>.5)-1;
        v=2*(rand(l,1)>.5)-1;
        M=v*h';
        Omeg= randperm(N*l,round(rate*N*l/100)); %    randi([1 N*l],1,round(rate*N*l/100)) is not unique
        R=zeros(size(M));
        for i=1:length(Omeg)
            R(Omeg(i))=M(Omeg(i));
        end
        
        

      % Generating quality  E and error matrix Z
        E=zeros(size(R));
        lambda=4;  % a metric for mizane noise   2> 126  4>28
        aa=poissrnd(lambda,length(Omeg),1);
        
        E(Omeg)=10.^(-aa);
        A=zeros(size(R));
        Z=zeros(size(R));
        for i=1:length(Omeg)
        A(Omeg(i)) = binornd(1, E(Omeg(i)) );
        if A(Omeg(i))==1
            Z(Omeg(i))=-2*M(Omeg(i));
        end
        end
        R_ex=R;
        R=R+Z;
        
        
        
        
       
        % R is the measurment matrix and M is the unkown matrix, E is given
        omg=find(R);
        %No weight
        W=zeros(size(R))+1;
        cvx_begin quiet
        variable X(N,l)
        minimize norm_nuc(X)
        subject to
        W(omg)'*(X(omg)-R(omg)).^2 <= .1; %norm(X(omg)-R(omg),2)<= .001;
        cvx_end
        M_ht=full(X);
        e(k)=norm(M_ht-M)/norm(M);
        
        %weighted
        W=zeros(size(R));
        W(omg)=log2(1./E(omg));
        cvx_begin quiet
        variable X(N,l)
        minimize norm_nuc(X)
        subject to
        W(omg)'*(X(omg)-R(omg)).^2 <= .1; %norm(X(omg)-R(omg),2)<= .001;
        cvx_end
        M_ht=full(X);
        e_w(k)=norm(M_ht-M)/norm(M);
      
    end
    

    ee(rate)=mean(e);
    ee_w(rate)=mean(e_w);
end



10*log10(mean(e))-10*log10(mean(e_w))

plot(Interval,(ee(Interval)),Interval,(ee_w(Interval)),'LineWidth',2)
legend('No weight','weighted')
title(' l=40 N=40  NosieRate=0.1 delta=.001')
xlabel('rate of known entries')
ylabel('normalized norm of error matrix')
